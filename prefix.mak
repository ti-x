# prefix makefile

# path seperator
ifeq ($(HOST), linux)
_ 				= /
endif

ifeq ($(HOST), windows)
_ 				= \\
endif

# path
SRC_DIR 		= $(PRO_DIR)$(_)src
BIN_DIR 		= $(PRO_DIR)$(_)bin$(_)$(PLAT)
INC_DIR 		= $(PRO_DIR)$(_)inc$(_)$(PLAT)
LIB_DIR 		= $(PRO_DIR)$(_)lib$(_)$(PLAT)
PLAT_DIR 		= $(PRO_DIR)$(_)plat$(_)$(PLAT)
PSRC_DIR 		= $(PRO_DIR)$(_)src$(_)pro

# exports
export 			_

# architecture makefile
-include 		$(PLAT_DIR)$(_)prefix.mak

