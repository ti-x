# config
IS_CONFIG =  yes

# architecture
PLAT =  arm_mini2440

# host
HOST =  linux

# root
PRO_DIR =  /home/ruki/projects/ti-x

# export
export PLAT
export HOST
export PRO_DIR
