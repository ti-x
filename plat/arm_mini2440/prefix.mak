# architecture makefile configure

# prefix & suffix
BIN_PREFIX 		= 
BIN_SUFFIX 		= 

OBJ_PREFIX 		= 
OBJ_SUFFIX 		= .o

LIB_PREFIX 		= lib
LIB_SUFFIX 		= .a

DLL_PREFIX 		= lib
DLL_SUFFIX 		= .so

ASM_SUFFIX 		= .S

# tool
PRE 			= arm-linux-
CC 				= $(PRE)gcc
AR 				= $(PRE)ar
LD 				= $(PRE)ld
STRIP 			= $(PRE)strip
RANLIB 			= $(PRE)ranlib
AS 				= $(PRE)gcc
RM 				= rm -f
RMDIR 			= rm -rf
CP 				= cp
CPDIR 			= cp -r
MKDIR 			= mkdir -p
MAKE 			= make
PWD 			= pwd

# cflags
#
# arm-linux-gcc 3.4.1 with softfloat: 		-mcpu=arm920t
# arm-linux-gcc 4.3.2 with hardfloat(vfp): 	-mcpu=arm920t -mfpu=vfp -mfloat-abi=softfp
#
CFLAGS 			= -c -O3 -g -Wall
#CFLAGS 			= -c -O3 -g -Wall -mcpu=arm920t
#CFLAGS 		= -c -O3 -g -Wall -mcpu=arm920t -mfpu=vfp -mfloat-abi=softfp

CFLAGS-I 		= -I
CFLAGS-o 		= -o

# ldflags
BOARD_DIR 		= $(SRC_DIR)$(_)arch$(_)$(TI_CONFIG_ARCH)$(_)board-$(TI_CONFIG_BOARD)
LDFLAGS 		= -static -T$(BOARD_DIR)$(_)ldscript.lds -L$(BOARD_DIR)
LDFLAGS-L 		= -L
LDFLAGS-l 		= -l
LDFLAGS-o 		= -o

# asflags
ASFLAGS 		= -c -O3 -g -Wall
ASFLAGS-I 		= -I
ASFLAGS-o 		= -o

# arflags
ARFLAGS 		= -rc

# share ldflags
SHFLAGS 		= -shared -Wl,-soname

# suffix command
# $(1): name
# $(2): type: BIN, LIB, DLL
ELF2BIN 		= $(PRE)objcopy -O binary -S $(1) $(1).bin
DUMPBIN 		= $(PRE)objdump -D -m arm $(1) > $(1).dis

# include sub-config
include 		$(PLAT_DIR)$(_)config.mak


