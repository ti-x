# # the ti-x embed operation system
# 
# it is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# it is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with it; If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>
# 
# Copyright (C) 2009, Ruki All rights reserved.
# Home: <a href="http://www.xxx.org">http://www.xxx.org</a>
#
# \author		ruki
# \date			09.11.20
# \file			config.mak
# \brief 		the config file
#
# #

# #################################################################
# config
# #

# ################################################################
# arch
# #

# arm
TI_CONFIG_ARCH 					= arm
TI_CONFIG_ARCH_ARM 				= y

# version
TI_CONFIG_ARM_V4 				= y
TI_CONFIG_ARM_V5 				= n
TI_CONFIG_ARM_V5TE 				= n
TI_CONFIG_ARM_V6 				= n


# ################################################################
# board
# #
TI_CONFIG_BOARD 				= mini2440
TI_CONFIG_BOARD_MINI2440 		= y


# ################################################################
# cpu
# #
TI_CONFIG_CPU 					= s3c2440
TI_CONFIG_CPU_CORE 				= arm920t
TI_CONFIG_CPU_S3C2440 			= y
TI_CONFIG_CPU_CORE_ARM920T 		= y

# ################################################################
# float
# #

# float
TI_CONFIG_FLOAT 				= y
TI_CONFIG_FLOAT_HARD 			= n
TI_CONFIG_FLOAT_SOFT 			= y

# arm embed application binary interface
TI_CONFIG_AEABI 				= n

