/* the ti-x embed operation system
 * 
 * it is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * it is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with it; If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>
 * 
 * Copyright (C) 2009, Ruki All rights reserved.
 * Home: <a href="http://www.xxx.org">http://www.xxx.org</a>
 *
 * \author		ruki
 * \date		09.11.20
 * \file		config.h
 * \brief 		the config file
 *
 */
#ifndef TI_CONFIG_H
#define TI_CONFIG_H

/* ////////////////////////////////////////////////////////////////
 * config
 */

// arch
#define TI_CONFIG_ARCH_ARM

// version
#define TI_CONFIG_ARM_VERSION 		(4)
#define TI_CONFIG_ARM_V4
//#define TI_CONFIG_ARM_V5
//#define TI_CONFIG_ARM_V5TE
//#define TI_CONFIG_ARM_V6

/* ////////////////////////////////////////////////////////////////
 * board
 */
// board
#define TI_CONFIG_BOARD_MINI2440

/* ////////////////////////////////////////////////////////////////
 * cpu
 */

// cpu
#define TI_CONFIG_CPU_S3C2440

// big endian & little endian
//#define TI_CONFIG_CPU_BIG_ENDIAN

/* ////////////////////////////////////////////////////////////////
 * cpu: s3c2440
 */

// the interrupt service routine
#define TI_CONFIG_S3C2440_ISR 				(0)
#define TI_CONFIG_S3C2440_ISR_UNDEF 		(0)
#define TI_CONFIG_S3C2440_ISR_SWI 			(0)
#define TI_CONFIG_S3C2440_ISR_PABORT 		(0)
#define TI_CONFIG_S3C2440_ISR_DABORT 		(0)
#define TI_CONFIG_S3C2440_ISR_IRQ 			(0)
#define TI_CONFIG_S3C2440_ISR_FIQ 			(0)

// clock
#define TI_CONFIG_S3C2440_CLOCK				(1)
#define TI_CONFIG_S3C2440_CLOCK_CPU_400MHz 	(1)
#define TI_CONFIG_S3C2440_CLOCK_USB_96MHz 	(0)
#define TI_CONFIG_S3C2440_CLOCK_USB_48MHz 	(1)


// stack
#define TI_CONFIG_S3C2440_UND_STACK_SIZE	(0x00000010)	// 16 bytes
#define TI_CONFIG_S3C2440_SVC_STACK_SIZE	(0x00000080)	// 128 bytes
#define TI_CONFIG_S3C2440_ABT_STACK_SIZE	(0x00000010)	// 16 bytes
#define TI_CONFIG_S3C2440_USR_STACK_SIZE	(0x00004000)	// 4096 bytes
#define TI_CONFIG_S3C2440_IRQ_STACK_SIZE	(0x00000040)	// 64 bytes
#define TI_CONFIG_S3C2440_FIQ_STACK_SIZE	(0x00000020)	// 32 bytes

/* ////////////////////////////////////////////////////////////////
 * serial
 */
// serial
#define TI_CONFIG_S3C2440_SERIAL0
//#define TI_CONFIG_S3C2440_SERIAL1
//#define TI_CONFIG_S3C2440_SERIAL2
#define TI_CONFIG_S3C2440_SERIAL_BAUDRATE 	(115200)

#endif
