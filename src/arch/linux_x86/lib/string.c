/* the ti-x embed operation system
 * 
 * it is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * it is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with it; If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>
 * 
 * Copyright (C) 2009, Ruki All rights reserved.
 * Home: <a href="http://www.xxx.org">http://www.xxx.org</a>
 *
 * \author		ruki
 * \date		09.12.06
 * \file		string.c
 *
 */

/* ////////////////////////////////////////////////////////////////////////
 * includes
 */
#include "prefix.h"
#include "stdio.h"
#include "string.h"

/* ////////////////////////////////////////////////////////////////////////
 * internal implemention
 */

// string length
ti_int_t __strlen(ti_char_t const* s)
{
	return strlen(s);
}
#if 0
ti_int_t __strnlen(ti_char_t const* s, ti_int_t n)
{
	return strnlen(s, n);
}
#endif

// string comparision
ti_int_t __strcmp(ti_char_t const* lhs, ti_char_t const* rhs)
{
	return strcmp(lhs, rhs);
}
ti_int_t __strncmp(ti_char_t const* lhs, ti_char_t const* rhs, ti_size_t n)
{
	return strncmp(lhs, rhs, n);
}

// string copy
ti_char_t const* __strcpy(ti_char_t* pd, ti_char_t const* ps)
{
	return strcpy(pd, ps);
}
ti_char_t const* __strncpy(ti_char_t* pd, ti_char_t const* ps, ti_size_t n)
{
	return strncpy(pd, ps, n);
}

