/* the ti-x embed operation system
 * 
 * it is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * it is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with it; If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>
 * 
 * Copyright (C) 2009, Ruki All rights reserved.
 * Home: <a href="http://www.xxx.org">http://www.xxx.org</a>
 *
 * \author		ruki
 * \date		09.12.06
 * \file		utils.c
 *
 */

/* ////////////////////////////////////////////////////////////////////////
 * includes
 */
#include "utils.h"

/* ////////////////////////////////////////////////////////////////////////
 * internal declaration
 */
ti_int_t 	__isdigit(ti_char_t c);

/* ////////////////////////////////////////////////////////////////////////
 * interface implemention
 */

ti_bool_t ti_isdigit(ti_char_t c)
{
#ifndef TI_ARCH_SPEC_HAVE_ISDIGIT
	return ((((ti_uint8_t)(c - '0')) < 10)? TI_TRUE : TI_FALSE);
#else
	return __isdigit(c)? TI_TRUE : TI_FALSE;
#endif
}

