/* the ti-x embed operation system
 * 
 * it is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * it is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with it; If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>
 * 
 * Copyright (C) 2009, Ruki All rights reserved.
 * Home: <a href="http://www.xxx.org">http://www.xxx.org</a>
 *
 * \author		ruki
 * \date		09.12.06
 * \file		memory.c
 *
 */

/* ////////////////////////////////////////////////////////////////////////
 * includes
 */
#include "memory.h"

/* ////////////////////////////////////////////////////////////////////////
 * internal declaration
 */
void* 	__memcpy(void* pd, void const* ps, ti_size_t n);
void* 	__memset(void* p, ti_byte_t b, ti_size_t n);

/* ////////////////////////////////////////////////////////////////////////
 * interface implemention
 */

ti_byte_t* ti_memcpy(ti_byte_t* pd, ti_byte_t const* ps, ti_size_t n)
{
#ifndef TI_ARCH_SPEC_HAVE_MEMCPY
	ti_byte_t* 			d = pd;
	ti_byte_t const* 	s = ps;

	while (n--) *d++ = *s++;
	return pd;
#else
	return __memcpy(pd, ps, n);
#endif
}

ti_byte_t* ti_memset(ti_byte_t* p, ti_byte_t b, ti_size_t n)
{
#ifndef TI_ARCH_SPEC_HAVE_MEMSET
	ti_byte_t* pb = p;
	while (n--) *p++ = b;
	return pb;
#else
	return __memset(p, b, n);
#endif
}
