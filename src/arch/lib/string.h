/* the ti-x embed operation system
 * 
 * it is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * it is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with it; If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>
 * 
 * Copyright (C) 2009, Ruki All rights reserved.
 * Home: <a href="http://www.xxx.org">http://www.xxx.org</a>
 *
 * \author		ruki
 * \date		09.12.06
 * \file		string.h
 *
 */
#ifndef TI_ARCH_LIB_STRING_H
#define TI_ARCH_LIB_STRING_H

// c plus plus
#ifdef __cplusplus
extern "C" {
#endif

/* ////////////////////////////////////////////////////////////////////////
 * includes
 */
#include "prefix.h"
#include "va_arg.h"

/* ////////////////////////////////////////////////////////////////////////
 * interfaces
 */

// string length
ti_int_t 			ti_strlen(ti_char_t const* s);
ti_int_t 			ti_strnlen(ti_char_t const* s, ti_int_t n);

// string comparision
ti_bool_t 			ti_strcmp(ti_char_t const* lhs, ti_char_t const* rhs);
ti_bool_t 			ti_strncmp(ti_char_t const* lhs, ti_char_t const* rhs, ti_size_t n);

// string copy
ti_char_t const* 	ti_strcpy(ti_char_t* pd, ti_char_t const* ps);
ti_char_t const* 	ti_strncpy(ti_char_t* pd, ti_char_t const* ps, ti_size_t n);


// format string
ti_int_t 			ti_vsnprintf(ti_char_t* s, ti_size_t n, ti_char_t const* fmt, ti_va_list_t args);
ti_int_t 			ti_vsprintf(ti_char_t* s, ti_char_t const* fmt, ti_va_list_t args);
ti_int_t 			ti_snprintf(ti_char_t* s, ti_size_t n, ti_char_t const* fmt, ...);
ti_int_t 			ti_sprintf(ti_char_t* s, ti_char_t const* fmt, ...);


// c plus plus
#ifdef __cplusplus
}
#endif
#endif

