/* the ti-x embed operation system
 * 
 * it is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * it is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with it; If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>
 * 
 * Copyright (C) 2009, Ruki All rights reserved.
 * Home: <a href="http://www.xxx.org">http://www.xxx.org</a>
 *
 * \author		ruki
 * \date		09.12.06
 * \file		console.h
 *
 */
#ifndef TI_ARCH_LIB_CONSOLE_H
#define TI_ARCH_LIB_CONSOLE_H

/* ////////////////////////////////////////////////////////////////////////
 * includes
 */
#include "prefix.h"

/* ////////////////////////////////////////////////////////////////////////
 * interfaces
 */

ti_char_t 		 	ti_getc();
ti_char_t 			ti_putc(ti_char_t c);

ti_char_t const* 	ti_gets(ti_char_t* s, ti_int_t* n);
ti_int_t 			ti_puts(ti_char_t const* s);

ti_int_t 			ti_printf(ti_char_t const* fmt, ...);


#endif
