/* the ti-x embed operation system
 * 
 * it is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * it is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with it; If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>
 * 
 * Copyright (C) 2009, Ruki All rights reserved.
 * Home: <a href="http://www.xxx.org">http://www.xxx.org</a>
 *
 * \author		ruki
 * \date		09.12.06
 * \file		console.c
 *
 */

/* ////////////////////////////////////////////////////////////////////////
 * includes
 */
#include "prefix.h"
#include "console.h"
#include "string.h"

/* ////////////////////////////////////////////////////////////////////////
 * internal declaration
 */
ti_char_t 			__getchar();
ti_char_t 			__putchar(ti_char_t c);
ti_int_t 			__gets(ti_char_t* s);
ti_int_t 			__puts(ti_char_t const* s);

/* ////////////////////////////////////////////////////////////////////////
 * interface implemention
 */
ti_char_t ti_getc()
{
#ifdef TI_ARCH_SPEC_HAVE_GETCHAR
	return __getchar();
#else
	return 0;
#endif
}
ti_char_t ti_putc(ti_char_t c)
{
#ifdef TI_ARCH_SPEC_HAVE_PUTCHAR
	return __putchar(c);
#else
	return 0;
#endif
}
ti_int_t ti_puts(ti_char_t const* s)
{
#ifndef TI_ARCH_SPEC_HAVE_PUTS
	ti_int_t n = 0;
	while (*s) 
	{
		ti_putc(*s++);
		++n;
	}
	return n;
#else
	return __puts(s);
#endif
}
ti_char_t const* ti_gets(ti_char_t* s, ti_int_t* n)
{
#ifndef TI_ARCH_SPEC_HAVE_GETS
	ti_char_t *s2 = s;
	ti_char_t c;
	if (n) *n = 0;
	while ((c = ti_getc()) != '\r')
	{
		if (c == '\b')
		{
			if ((ti_int_t)s2 < (ti_int_t)s)
			{
				ti_puts("\b \b");
				s--;
				if (n) --(*n);
			}
		}
		else 
		{
			*s++ = c;
			ti_putc(c);
			if (n) ++(*n);
		}
	}
	*s = '\0';
	ti_putc('\n');
	return s2;
#else
	ti_int_t size = __gets(s);
	if (n) *n = size;
	return s;
#endif
}

ti_int_t ti_printf(ti_char_t const* fmt, ...)
{
	ti_va_list_t args;
	ti_int_t i;

	ti_char_t s[512];
	TI_VA_START(args, fmt);
	i = ti_vsnprintf(s, 512 - 1, fmt, args);
	TI_VA_END(args);
	s[512 - 1] = '\0';

	ti_puts(s);
	return i;
}
