/* the ti-x embed operation system
 * 
 * it is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * it is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with it; If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>
 * 
 * Copyright (C) 2009, Ruki All rights reserved.
 * Home: <a href="http://www.xxx.org">http://www.xxx.org</a>
 *
 * \author		ruki
 * \date		09.12.06
 * \file		string.c
 *
 */

/* ////////////////////////////////////////////////////////////////////////
 * includes
 */
#include "string.h"
#include "memory.h"
#include "limits.h"
#include "utils.h"

/* ////////////////////////////////////////////////////////////////////////
 * types
 */

// printf format type
typedef enum __ti_printf_type_t
{
	TI_PRINTF_TYPE_NONE
, 	TI_PRINTF_TYPE_INT
, 	TI_PRINTF_TYPE_CHAR
, 	TI_PRINTF_TYPE_CHAR_PERCENT
, 	TI_PRINTF_TYPE_FLOAT
, 	TI_PRINTF_TYPE_STRING
, 	TI_PRINTF_TYPE_WIDTH
, 	TI_PRINTF_TYPE_PRECISION
, 	TI_PRINTF_TYPE_INVALID

}ti_printf_type_t;

// printf format extra info
typedef enum __ti_printf_extra_t
{
	TI_PRINTF_EXTRA_NONE 	= 0
, 	TI_PRINTF_EXTRA_SIGNED 	= 1 	// signed integer for %d %i
, 	TI_PRINTF_EXTRA_UPPER 	= 2 	// upper case for %X %B
, 	TI_PRINTF_EXTRA_PERCENT = 4 	// percent char: %
, 	TI_PRINTF_EXTRA_EXP 	= 8 	// exponent form: [-]d.ddd e[+/-]ddd

}ti_printf_extra_t;

// printf length qualifier
typedef enum __ti_printf_qual_t
{
	TI_PRINTF_QUAL_NONE
, 	TI_PRINTF_QUAL_I
, 	TI_PRINTF_QUAL_I8
, 	TI_PRINTF_QUAL_I16
, 	TI_PRINTF_QUAL_I32
, 	TI_PRINTF_QUAL_I64

}ti_printf_qual_t;

// printf flag type
typedef enum __ti_printf_flag_t
{
	TI_PRINTF_FLAG_NONE		= 0
, 	TI_PRINTF_FLAG_PLUS 	= 1 	// +: denote the sign '+' or '-' of a number
, 	TI_PRINTF_FLAG_LEFT 	= 2 	// -: left-justified
, 	TI_PRINTF_FLAG_ZERO 	= 4 	// 0: fill 0 instead of spaces
, 	TI_PRINTF_FLAG_PFIX 	= 8 	// #: add prefix 

}ti_printf_flag_t;

// printf entry
typedef struct __ti_printf_entry_t
{
	// format type
	ti_printf_type_t 	type;

	// extra info 
	ti_printf_extra_t 	extra;

	// flag
	ti_printf_flag_t 	flags;

	// field width
	ti_int_t 			width;

	// precision
	ti_int_t 			precision;

	// qualifier
	ti_printf_qual_t 	qual;

	// base: 2 8 10 16 
	ti_int_t 			base;


}ti_printf_entry_t;

/* ////////////////////////////////////////////////////////////////////////
 * internal declaration
 */

// string length
ti_int_t 			__strlen(ti_char_t const* s);
ti_int_t 			__strnlen(ti_char_t const* s, ti_int_t n);

// string comparision
ti_int_t 			__strcmp(ti_char_t const* lhs, ti_char_t const* rhs);
ti_int_t 			__strncmp(ti_char_t const* lhs, ti_char_t const* rhs, ti_size_t n);

// string copy
ti_char_t const* 	__strcpy(ti_char_t* pd, ti_char_t const* ps);
ti_char_t const* 	__strncpy(ti_char_t* pd, ti_char_t const* ps, ti_size_t n);

ti_int_t 			__vsnprintf(ti_char_t* s, ti_size_t n, ti_char_t const* fmt, ti_va_list_t args);
ti_int_t 			__vsprintf(ti_char_t* s, ti_char_t const* fmt, ti_va_list_t args);

/* ////////////////////////////////////////////////////////////////////////
 * details
 */

static ti_int_t ti_skip_atoi(ti_char_t const** s)
{
	ti_int_t i = 0;
	while (ti_isdigit(**s) == TI_TRUE) 
		i = i * 10 + *((*s)++) - '0';
	return i;
}
static ti_char_t* ti_printf_string(ti_char_t* pb, ti_char_t* pe, ti_printf_entry_t e, ti_char_t const* s)
{
	ti_int_t n = ti_strnlen(s, e.precision);

	// fill space at left side, e.g. "   abcd"
	if (!(e.flags & TI_PRINTF_FLAG_LEFT)) 
	{
		while (n < e.width--) 
			if (pb < pe) *pb++ = ' ';
	}

	// copy string
	ti_int_t i = 0;
	for (i = 0; i < n; ++i)
		if (pb < pe) *pb++ = *s++;

	// fill space at right side, e.g. "abcd    "
	while (n < e.width--) 
		if (pb < pe) *pb++ = ' ';

	return pb;
}
static ti_char_t* ti_printf_integer(ti_char_t* pb, ti_char_t* pe, ti_printf_entry_t e, ti_uint64_t num)
{
	// digits table
	static ti_char_t const digits_table[16] = "0123456789ABCDEF";

	// max: 64-bit binary decimal
	ti_char_t 	digits[64] = {0};
	ti_int_t 	digit_i = 0;

	// lowercase mask, e.g. 'F' | 0x20 => 'f'
	ti_int_t lomask = (e.extra & TI_PRINTF_EXTRA_UPPER)? 0x0 : 0x20;

	// sign: + -
	ti_char_t sign = 0;
	if (e.extra & TI_PRINTF_EXTRA_SIGNED)
	{
		if ((ti_int64_t)num < 0) 
		{
			sign = '-';
			--e.width;
		}
		else if (e.flags & TI_PRINTF_FLAG_PLUS)
		{
			sign = '+';
			--e.width;
		}
	}

	// convert num => digits string in reverse order
	if (num == 0) digits[digit_i++] = '0';
	else 
	{
		if ((ti_int64_t)num < 0) num = (ti_uint64_t)(-(ti_int64_t)num); 

#if 0
		do 
		{
			digits[digit_i++] = digits_table[num % e.base] | lomask;
			num /= e.base;
		}
		while (num);
#else
		if (e.base != 10)
		{
			ti_int_t shift_bits = 4;
			if (e.base == 8) shift_bits--;
			else if (e.base == 2) shift_bits -= 3;
			do 
			{
				digits[digit_i++] = digits_table[(ti_uint8_t)num & (e.base - 1)] | lomask;
				num >>= shift_bits;
			}
			while (num);
		}
		else
		{
			do 
			{
				digits[digit_i++] = digits_table[num % e.base] | lomask;
				num /= e.base;
			}
			while (num);
		}
#endif
	}

	// adjust precision
	if (digit_i > e.precision) 
		e.precision = digit_i;

	// fill spaces at left side, e.g. "   0x0"
	e.width -= e.precision;
	if (!(e.flags & (TI_PRINTF_FLAG_LEFT + TI_PRINTF_FLAG_ZERO)))
	{
		while (--e.width >= 0)
			if (pb < pe) *pb++ = ' ';
	}

	// append sign: + / -
	if (sign && (pb < pe)) *pb++ = sign;

	// append prefix: 0x..., 0X..., 0b..., 0B...
	if (e.flags & TI_PRINTF_FLAG_PFIX)
	{
		switch (e.base)
		{
		case 16:
			{
				if (pb + 1 < pe) 
				{
					*pb++ = '0';
					*pb++ = 'X' | lomask;
					e.width -= 2;
				}
				break;
			}
		case 8:
			{
				if (pb < pe) 
				{
					*pb++ = '0';
					--e.width;
				}
				break;
			}
		case 2:
			{
				if (pb + 1 < pe) 
				{
					*pb++ = '0';
					*pb++ = 'B' | lomask;
					e.width -= 2;
				}
				break;
			}
		default:
			break;
		}
	}

	// fill 0 or spaces, e.g. "0x   ff"
	if (!(e.flags & TI_PRINTF_FLAG_LEFT))
	{
		ti_char_t c = (e.flags & TI_PRINTF_FLAG_ZERO)? '0' : ' ';
		while (--e.width >= 0)
			if (pb < pe) *pb++ = c;
	}

	// fill 0 if precision is larger, e.g. "0x000ff"
	while (digit_i <= --e.precision) 
		if (pb < pe) *pb++ = '0';

	// append digits
	while (--digit_i >= 0) 
		if (pb < pe) *pb++ = digits[digit_i];

	// trailing space padding for left-justified flags, e.g. "0xff   "
	while (--e.width >= 0)
		if (pb < pe) *pb++ = ' ';

	return pb;
}
#ifdef TI_ARCH_HAVE_FLOAT
// only support double float-point 
static ti_char_t* ti_printf_float(ti_char_t* pb, ti_char_t* pe, ti_printf_entry_t e, ti_float_t num)
{
	// digits
	ti_char_t 	ints[64] = {0};
	ti_char_t 	decs[64] = {0};
	ti_int_t 	ints_i = 0, decs_i = 0;

	// sign: + -
	ti_char_t sign = 0;
	if (e.extra & TI_PRINTF_EXTRA_SIGNED)
	{
		if (num < 0) 
		{
			sign = '-';
			--e.width;
		}
		else if (e.flags & TI_PRINTF_FLAG_PLUS)
		{
			sign = '+';
			--e.width;
		}
	}

	// adjust sign
	if (num < 0) num = -num;

	// get integer & decimal
	ti_int64_t integer = (ti_int64_t)num;
	ti_float_t decimal = num - integer;

	// convert integer => digits string in reverse order
	if (integer == 0) ints[ints_i++] = '0';
	else 
	{
		if (integer < 0) integer = -integer; 
		do 
		{
			ints[ints_i++] = (integer % 10) + '0';
			integer /= 10;
		}
		while (integer);
	}

	// default precision: 6
	if (e.precision <= 0) e.precision = 6;

	// convert decimal => digits string in positive order
	if (decimal == 0) decs[decs_i++] = '0';
	else 
	{
		ti_int_t d = (ti_int_t)(decimal * 10);
		do 
		{
			decs[decs_i++] = d + '0';
			decimal = decimal * 10 - d;
			d = (ti_int_t)(decimal * 10);
		}
		while (decs_i < e.precision);
	}

	// adjust precision
	if (decs_i > e.precision) 
		decs_i = e.precision;

	// fill spaces at left side, e.g. "   0.31415926"
	e.width -= ints_i + 1 + e.precision;
	if (!(e.flags & (TI_PRINTF_FLAG_LEFT + TI_PRINTF_FLAG_ZERO)))
	{
		while (--e.width >= 0)
			if (pb < pe) *pb++ = ' ';
	}

	// append sign: + / -
	if (sign && (pb < pe)) *pb++ = sign;

	// fill 0 or spaces, e.g. "00003.1415926"
	if (!(e.flags & TI_PRINTF_FLAG_LEFT))
	{
		ti_char_t c = (e.flags & TI_PRINTF_FLAG_ZERO)? '0' : ' ';
		while (--e.width >= 0)
			if (pb < pe) *pb++ = c;
	}

	// append integer
	while (--ints_i >= 0) 
		if (pb < pe) *pb++ = ints[ints_i];

	// append .
	if (pb < pe) *pb++ = '.';

	// append decimal
	ti_int_t decs_n = decs_i;
	while (--decs_i >= 0) 
		if (pb < pe) *pb++ = decs[decs_n - decs_i - 1];

	// fill 0 if precision is larger, e.g. "0.3140000"
	while (decs_n <= --e.precision) 
		if (pb < pe) *pb++ = '0';

	// trailing space padding for left-justified flags, e.g. "0.31415926   "
	while (--e.width >= 0)
		if (pb < pe) *pb++ = ' ';

	return pb;
}
#endif
// get a printf format entry
static ti_int_t ti_printf_entry(ti_char_t const* fmt, ti_printf_entry_t* e)
{
	ti_char_t const* p = fmt;

	// get field width for *
	if (e->type == TI_PRINTF_TYPE_WIDTH)
	{
		if (e->width < 0) 
		{
			e->width = -e->width;
			e->flags |= TI_PRINTF_FLAG_LEFT;
		}
		e->type = TI_PRINTF_TYPE_NONE;
		goto get_precision;
	}

	// get precision for *
	if (e->type == TI_PRINTF_TYPE_PRECISION)
	{
		if (e->precision < 0) e->precision = 0;
		e->type = TI_PRINTF_TYPE_NONE;
		goto get_qualifier;
	}

	// default type
	e->type = TI_PRINTF_TYPE_NONE;

	// goto %
	for (; *p; ++p) 
		if (*p == '%') break;

	// return non-format string
	if (p != fmt || !*p)
		return (p - fmt);

	// skip %
	++p;

	// get flags
	e->flags = TI_PRINTF_FLAG_NONE;
	while (1)
	{
		ti_bool_t is_found = TI_TRUE;
		switch (*p)
		{
		case '+': e->flags |= TI_PRINTF_FLAG_PLUS; break;
		case '-': e->flags |= TI_PRINTF_FLAG_LEFT; break;
		case '0': e->flags |= TI_PRINTF_FLAG_ZERO; break;
		case '#': e->flags |= TI_PRINTF_FLAG_PFIX; break;
		default: is_found = TI_FALSE; break;
		}
		if (is_found == TI_FALSE) break;
		else ++p;
	}

	// get field width
	e->width = -1;
	if (ti_isdigit(*p) == TI_TRUE) e->width = ti_skip_atoi(&p);
	else if (*p == '*') 
	{
		// it's the next argument
		e->type = TI_PRINTF_TYPE_WIDTH;
		return ++p - fmt;
	}

get_precision:
	// get precision
	e->precision = -1;
	if (*p == '.')
	{
		++p;
		if (ti_isdigit(*p) == TI_TRUE) 
		{
			e->precision = ti_skip_atoi(&p);
			if (e->precision < 0) e->precision = 0;
		}
		else if (*p == '*') 
		{
			// it's the next argument
			e->type = TI_PRINTF_TYPE_PRECISION;
			return ++p - fmt;
		}
	}

get_qualifier:
	// get length qualifier
	e->qual = TI_PRINTF_QUAL_NONE;
	switch (*p)
	{
		// short & long => int
	case 'h':
	case 'l':
		++p;
		if (*p == 'l') 
		{
			e->qual = TI_PRINTF_QUAL_I64;
			++p;
		}
		else e->qual = TI_PRINTF_QUAL_I;
		break;
	case 'I':
		{
			++p;
			ti_int_t n = ti_skip_atoi(&p);
			switch (n)
			{
			case 8: e->qual = TI_PRINTF_QUAL_I8; break;
			case 16: e->qual = TI_PRINTF_QUAL_I16; break;
			case 32: e->qual = TI_PRINTF_QUAL_I32; break;
			case 64: e->qual = TI_PRINTF_QUAL_I64; break;
			default: e->qual = TI_PRINTF_QUAL_I; break;
			}
			break;
		}
	default:
		e->qual = TI_PRINTF_QUAL_NONE;
		break;
	}

	// get base & type
	e->base = -1;
	e->type = TI_PRINTF_TYPE_INVALID;
	e->extra = TI_PRINTF_EXTRA_NONE;
	switch (*p)
	{
	case 's':
		e->type = TI_PRINTF_TYPE_STRING;
		return (++p - fmt);
	case '%':
		e->extra |= TI_PRINTF_EXTRA_PERCENT;
	case 'c':
		e->type = TI_PRINTF_TYPE_CHAR;
		return (++p - fmt);
	case 'd':
	case 'i':
		e->extra |= TI_PRINTF_EXTRA_SIGNED;
	case 'u':
		e->base = 10;
		e->type = TI_PRINTF_TYPE_INT;
		break;
	case 'X':
		e->extra |= TI_PRINTF_EXTRA_UPPER;
	case 'x':
		e->base = 16;
		e->type = TI_PRINTF_TYPE_INT;
		break;
	case 'o':
		e->base = 8;
		e->type = TI_PRINTF_TYPE_INT;
		break;
	case 'B':
		e->extra |= TI_PRINTF_EXTRA_UPPER;
	case 'b':
		e->base = 2;
		e->type = TI_PRINTF_TYPE_INT;
		break;
#ifdef TI_CONFIG_FLOAT
	case 'F':
		e->extra |= TI_PRINTF_EXTRA_UPPER;
	case 'f':
		e->type = TI_PRINTF_TYPE_FLOAT;
		e->extra |= TI_PRINTF_EXTRA_SIGNED;
		break;
	case 'E':
		e->extra |= TI_PRINTF_EXTRA_UPPER;
	case 'e':
		e->type = TI_PRINTF_TYPE_FLOAT;
		e->extra |= TI_PRINTF_EXTRA_SIGNED;
		e->extra |= TI_PRINTF_EXTRA_EXP;
		break;
#endif
	default:
		e->type = TI_PRINTF_TYPE_INVALID;
		return (p - fmt);
	}

	return (++p - fmt);
}

/* ////////////////////////////////////////////////////////////////////////
 * interface implemetion
 */

ti_int_t ti_strlen(ti_char_t const* s)
{
#ifndef TI_ARCH_SPEC_HAVE_STRLEN
	ti_char_t const* ps;
	for (ps = s; *ps != '\0'; ++ps) ;
	return (ps - s);
#else
	return __strlen(s);
#endif
}
ti_int_t ti_strnlen(ti_char_t const* s, ti_int_t n)
{
#ifndef TI_ARCH_SPEC_HAVE_STRNLEN
	ti_char_t const* ps;
	for (ps = s; n-- && *ps != '\0'; ++ps) ;
	return (ps - s);
#else
	return __strnlen(s, n);
#endif
}
ti_char_t const* ti_strcpy(ti_char_t* pd, ti_char_t const* ps)
{
#ifndef TI_ARCH_SPEC_HAVE_STRCPY
	ti_char_t const* p = pd;
	while ((*pd++ = *ps++) != '\0') ;
	return p;
#else
	return __strcpy(pd, ps);
#endif
}
ti_char_t const* ti_strncpy(ti_char_t* pd, ti_char_t const* ps, ti_size_t n)
{
#ifndef TI_ARCH_SPEC_HAVE_STRNCPY
	ti_char_t const* p = pd;
	while (n) 
	{
		if ((*pd = *ps) != 0) ps++;
		pd++;
		n--;
	}
	return p;
#else
	return __strncpy(pd, ps, n);
#endif
}
ti_bool_t ti_strcmp(ti_char_t const* lhs, ti_char_t const* rhs)
{
#ifndef TI_ARCH_SPEC_HAVE_STRCMP
	ti_int_t ret;
	while (1) 
	{
		if ((ret = *lhs - *rhs++) != 0 || !*lhs++)
			break;
	}
	return ((ret == 0)? TI_TRUE : TI_FALSE);
#else
	return ((__strcmp(lhs, rhs) == 0)? TI_TRUE : TI_FALSE);
#endif
}
ti_bool_t ti_strncmp(ti_char_t const* lhs, ti_char_t const* rhs, ti_size_t n)
{
#ifndef TI_ARCH_SPEC_HAVE_STRNCMP
	ti_int_t ret = -1;
	while (n) 
	{
		if ((ret = *lhs - *rhs++) != 0 || !*lhs++)
			break;
		n--;
	}
	return ((ret == 0)? TI_TRUE : TI_FALSE);
#else
	return ((__strncmp(lhs, rhs, n) == 0)? TI_TRUE : TI_FALSE);
#endif
}

/*! format a string and place it in a buffer
 *
 * \param s: the buffer to place the result into
 * \param n: the size of the buffer, including the trailing null space
 * \param fmt: the format string to use
 * \param args: arguments for the format string
 *
 * return the number of characters which would be generated for the given input, 
 * excluding the trailing '\0'.
 *
 * format: %[flags][width][.precision][qualifier]type
 *
 * - flags:
 *   - default: right-justified, left-pad the output with spaces until the required length of output is attained. 
 *   			If combined with '0' (see below), 
 *   			it will cause the sign to become a space when positive, 
 *   			but the remaining characters will be zero-padded
 *   - -: 		left-justified, e.g. %-d
 *   - +: 		denote the sign '+' or '-' of a number
 *   - 0: 		use 0 instead of spaces to left-fill a fixed-length field
 *   - #: 		add prefix or suffix	
 *     - %#o => add prefix: 0...
 *     - %#x => add prefix: 0x...
 *     - %#X => add prefix: 0X...
 *     - %#b => add prefix: 0b...
 *     - %#B => add prefix: 0B...
 *     - %#f => add prefix: 0f...
 *     - %#F => add prefix: 0F...
 *
 * - width:
 *   - n: 		n = 1, 2, 3, ..., fill spaces
 *   - 0n: 		n = 1, 2, 3, ..., fill 0
 *   - *: 		causes printf to pad the output until it is n characters wide, 
 *   			where n is an integer value stored in the a function argument just preceding 
 *   			that represented by the modified type. 
 *   			e.g. printf("%*d", 5, 10) will result in "10" being printed with a width of 5.
 *
 * - .precision:
 *   - .n: 		for non-integral numeric types, causes the decimal portion of the output to be expressed in at least number digits. 
 *   			for the string type, causes the output to be truncated at number characters. 
 *   			if the precision is zero, nothing is printed for the corresponding argument.
 *   - *: 		same as the above, but uses an integer value in the intaken argument to 
 *   			determine the number of decimal places or maximum string length. 
 *   			e.g. printf("%.*s", 3, "abcdef") will result in "abc" being printed.
 *
 * - qualifier:
 *   - h: 		short integer or single float-point
 *   - l: 		long integer or double float-point
 *   - I8: 		8-bit integer
 *   - I16: 	16-bit integer
 *   - I32: 	32-bit integer
 *   - I64/ll: 	64-bit integer
 *
 * \note:
 * support: 		h, l, I8, I16, I32, I64, ll
 * not support: 	
 *
 * - type(e.g. %d %x %u %% ...):
 *   - d, i: 	print an int as a signed decimal number. 
 *   			'%d' and '%i' are synonymous for output, but are different when used with scanf() for input.
 *   - u: 		print decimal unsigned int.
 *   - o: 		print an unsigned int in octal.
 *   - x/X: 	print an unsigned int as a hexadecimal number. 'x' uses lower-case letters and 'X' uses upper-case.
 *   - b/B: 	print an unsigned binary interger
 *   - e/E: 	print a double value in standard form ([-]d.ddd e[+/-]ddd).
 *   			An E conversion uses the letter E (rather than e) to introduce the exponent. 
 *  			The exponent always contains at least two digits; if the value is zero, the exponent is 00.
 *  			e.g. 3.141593e+00
 *   - f/F: 	Print a double in normal (fixed-point) notation. 
 *   			'f' and 'F' only differs in how the strings for an infinite number or NaN are printed 
 *  			('inf', 'infinity' and 'nan' for 'f', 'INF', 'INFINITY' and 'NAN' for 'F').
 *   - g/G: 	print a double in either normal or exponential notation, whichever is more appropriate for its magnitude. 
 *   			'g' uses lower-case letters, 'G' uses upper-case letters. 
 *   			This type differs slightly from fixed-point notation in 
 *   			that insignificant zeroes to the right of the decimal point are not included. 
 *   			Also, the decimal point is not included on whole numbers.
 *   - c: 		print a char (character).
 *   - s: 		print a character string
 *   - p: 		print a void * (pointer to void) in an implementation-defined format.
 *   - n: 		print nothing, but write number of characters successfully written so far into an integer pointer parameter.
 *   - %: 		%
 *
 * \note: 
 * support: 		d, i, u, o, u, x/X, b/B, f/F, c, s, %
 * not support: 	e/E, g/G, p, n
 *
 * e.g.
 *  
 *  ti_printf("|hello world|\n");
	ti_printf("|%-10s|%%|%10s|\n", "hello", "world");
	ti_printf("|%#2c|%2.5c|%*c|\n", 'A', 'B', 5, 'C');
	ti_printf("|%#2d|%#8.3o|%*.*d|\n", -56, 56, 10, 5, 56);
	ti_printf("|%#-8.5x|%#2.9X|\n", 0x1f, 0x1f);
	ti_printf("|%#-8.5b|%#2.9B|\n", 0x1f, 0x1f);
	ti_printf("|%-6Id|%5I8u|%#I64x|%#llx|\n", 256, 255, (ti_int64_t)0x8fffffffffff, (ti_int64_t)0x8fffffffffff);
	ti_printf("|%f|\n", -3.1415926535897932384626433832795);
	ti_printf("|%f|%f|%f|\n", 3.14, 0, -0);
	ti_printf("|%0.9f|\n", 3.1415926535897932384626433832795);
	ti_printf("|%16.9f|\n", 3.1415926535897932384626433832795);
	ti_printf("|%016.9f|\n", 3.14159);
	ti_printf("|%lf|\n", 1.0 / 6.0);
	ti_printf("|%f|\n", 0.0003141596);
 *
 */
ti_int_t ti_vsnprintf(ti_char_t* s, ti_size_t n, ti_char_t const* fmt, ti_va_list_t args)
{
#ifndef TI_ARCH_SPEC_HAVE_VSNPRINTF
	if ((ti_int_t)n < 0 || !s || !fmt) return 0;

	ti_char_t* pb = s;
	ti_char_t* pe = s + n;

	// pe must be larger than pb
	if (pe < pb) 
	{
		pe = ((ti_char_t*)-1);
		n = (ti_size_t)(pe - pb);
	}

	// parse format
	ti_printf_entry_t e = {0};
	ti_int_t en = 0;
	while (*fmt)
	{
		ti_char_t const* ofmt = fmt;

		// get an entry
		en = ti_printf_entry(fmt, &e);
		fmt += en;

		switch (e.type)
		{
			// copy it if none type
		case TI_PRINTF_TYPE_NONE:
			{
				ti_int_t copy_n = en;
				if (pb < pe) 
				{
					if (copy_n > pe - pb) copy_n = pe - pb;
					ti_memcpy((ti_byte_t*)pb, (ti_byte_t const*)ofmt, copy_n);
					pb += copy_n;
				}
				break;
			}
			// get a character for %c
		case TI_PRINTF_TYPE_CHAR:
			{
				// char: %
				if (e.extra & TI_PRINTF_EXTRA_PERCENT)
				{
					if (pb < pe) *pb++ = '%';
				}
				// char: %c
				else
				{
					// fill space at left side, e.g. "   a"
					if (!(e.flags & TI_PRINTF_FLAG_LEFT)) 
					{
						while (--e.width > 0) 
						{
							if (pb < pe) *pb++ = ' ';
						}
					}

					if (pb < pe) *pb++ = (ti_char_t)TI_VA_ARG(args, ti_int_t);

					// fill space at right side, e.g. "a   "
					while (--e.width > 0) 
					{
						if (pb < pe) *pb++ = ' ';
					}
				}
				break;
			}
			// get field width for *
		case TI_PRINTF_TYPE_WIDTH:
			e.width = TI_VA_ARG(args, ti_int_t);
			break;
			// get precision for *
		case TI_PRINTF_TYPE_PRECISION:
			e.precision = TI_VA_ARG(args, ti_int_t);
			break;
			// get string for %s
		case TI_PRINTF_TYPE_STRING:
			{
				pb = ti_printf_string(pb, pe, e, TI_VA_ARG(args, ti_char_t const*));
				break;
			}
			// get an integer for %d %u %x ...
		case TI_PRINTF_TYPE_INT:
			{
				ti_uint64_t num = 0;
				if (e.extra & TI_PRINTF_EXTRA_SIGNED)
				{
					switch (e.qual)
					{
					case TI_PRINTF_QUAL_I: 		num = TI_VA_ARG(args, ti_int_t); break;
					case TI_PRINTF_QUAL_I8:		num = (ti_int8_t)TI_VA_ARG(args, ti_int_t); break;
					case TI_PRINTF_QUAL_I16:	num = (ti_int16_t)TI_VA_ARG(args, ti_int_t); break;
					case TI_PRINTF_QUAL_I32:	num = TI_VA_ARG(args, ti_int32_t); break;
					case TI_PRINTF_QUAL_I64:	num = TI_VA_ARG(args, ti_int64_t); break;
					default: 					num = TI_VA_ARG(args, ti_int_t); break;
					}
				}
				else
				{
					switch (e.qual)
					{
					case TI_PRINTF_QUAL_I: 		num = TI_VA_ARG(args, ti_uint_t); break;
					case TI_PRINTF_QUAL_I8:		num = (ti_uint8_t)TI_VA_ARG(args, ti_uint_t); break;
					case TI_PRINTF_QUAL_I16:	num = (ti_uint16_t)TI_VA_ARG(args, ti_uint_t); break;
					case TI_PRINTF_QUAL_I32:	num = TI_VA_ARG(args, ti_uint32_t); break;
					case TI_PRINTF_QUAL_I64:	num = TI_VA_ARG(args, ti_uint64_t); break;
					default: 					num = TI_VA_ARG(args, ti_uint_t); break;
					}
				}
				pb = ti_printf_integer(pb, pe, e, num);
				break;
			}
#ifdef TI_CONFIG_FLOAT
		case TI_PRINTF_TYPE_FLOAT:
			{
				ti_float_t num = TI_VA_ARG(args, ti_float_t);
				pb = ti_printf_float(pb, pe, e, num);
				break;
			}
#endif
		case TI_PRINTF_TYPE_INVALID:
			{
				if (pb < pe) *pb++ = '%';
				break;
			}
		default:
			break;
		}
	}

	// != 0, the string is null-truncated
	if (n > 0) 
	{
		if (pb < pe) *pb = '\0';
		else pe[-1] = '\0';
	}

	// the trailing null byte doesn't count towards the total
	return (pb - s);
#else
	return __vsnprintf(s, n, fmt, args);
#endif
}
ti_int_t ti_vsprintf(ti_char_t* s, ti_char_t const* fmt, ti_va_list_t args)
{
#ifndef TI_ARCH_SPEC_HAVE_VSPRINTF
	return ti_vsnprintf(s, TI_INT_MAX, fmt, args);
#else
	return __vsprintf(s, fmt, args);
#endif
}
ti_int_t ti_snprintf(ti_char_t* s, ti_size_t n, ti_char_t const* fmt, ...)
{
	ti_va_list_t args;
	ti_int_t i;

	TI_VA_START(args, fmt);
	i = ti_vsnprintf(s, n, fmt, args);
	TI_VA_END(args);
	
	return i;
}
ti_int_t ti_sprintf(ti_char_t* s, ti_char_t const* fmt, ...)
{
	ti_va_list_t args;
	ti_int_t i;

	TI_VA_START(args, fmt);
	i = ti_vsnprintf(s, TI_INT_MAX, fmt, args);
	TI_VA_END(args);

	return i;
}
