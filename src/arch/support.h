/* the ti-x embed operation system
 * 
 * it is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * it is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with it; If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>
 * 
 * Copyright (C) 2009, Ruki All rights reserved.
 * Home: <a href="http://www.xxx.org">http://www.xxx.org</a>
 *
 * \author		ruki
 * \date		09.11.20
 * \file		support.h
 *
 */
#ifndef TI_ARCH_SUPPORT_H
#define TI_ARCH_SUPPORT_H

/* ////////////////////////////////////////////////////////////////////////
 * includes
 */
#include "prefix.h"
#include "lib/support.h"

/* ////////////////////////////////////////////////////////////////
 * arch-support
 */
#if defined(TI_CONFIG_ARCH_ARM)
# 	include "arm/support.h"
#elif defined(TI_CONFIG_ARCH_LINUX_x86)
# 	include "linux_x86/support.h"
#else
# 	error unknown architecture
#endif

#endif
