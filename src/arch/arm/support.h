/* the ti-x embed operation system
 * 
 * it is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * it is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with it; If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>
 * 
 * Copyright (C) 2009, Ruki All rights reserved.
 * Home: <a href="http://www.xxx.org">http://www.xxx.org</a>
 *
 * \author		ruki
 * \date		09.11.20
 * \file		support.h
 *
 */
#ifndef TI_ARCH_ARM_SUPPORT_H
#define TI_ARCH_ARM_SUPPORT_H

/* ////////////////////////////////////////////////////////////////////////
 * includes
 */
#include "prefix.h"

/* ////////////////////////////////////////////////////////////////
 * cpu-support
 */
#if defined(TI_CONFIG_CPU_S3C2440)
# 	include "cpu-s3c2440/support.h"
#endif

// arm embed application binary interface
//#define TI_ARCH_HAVE_AEABI

/* ////////////////////////////////////////////////////////////////////////
 * c runtime library support, specific architecture
 */

/* ////////////////////////////////////////////////////////////////////////
 * memory operation
 */
// memcpy
//#define TI_ARCH_SPEC_HAVE_MEMCPY
// memset
#define TI_ARCH_SPEC_HAVE_MEMSET

/* ////////////////////////////////////////////////////////////////////////
 * string operation
 */
// strlen
//#define TI_ARCH_SPEC_HAVE_STRLEN
// strnen
//#define TI_ARCH_SPEC_HAVE_STRNLEN
// strcmp
//#define TI_ARCH_SPEC_HAVE_STRCMP
// strncmp
//#define TI_ARCH_SPEC_HAVE_STRNCMP
// strcpy
//#define TI_ARCH_SPEC_HAVE_STRCPY
// strncpy
//#define TI_ARCH_SPEC_HAVE_STRNCPY
//
/* ////////////////////////////////////////////////////////////////////////
 * console operation
 */
// getchar
#define TI_ARCH_SPEC_HAVE_GETCHAR
// putchar
#define TI_ARCH_SPEC_HAVE_PUTCHAR
// gets
//#define TI_ARCH_SPEC_HAVE_GETS
// puts
//#define TI_ARCH_SPEC_HAVE_PUTS

/* ////////////////////////////////////////////////////////////////////////
 * utils operation
 */
// isdigit
//#define TI_ARCH_SPEC_HAVE_ISDIGIT


#endif
