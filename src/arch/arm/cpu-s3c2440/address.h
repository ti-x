/* the ti-x embed operation system
 * 
 * it is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * it is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with it; If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>
 * 
 * Copyright (C) 2009, Ruki All rights reserved.
 * Home: <a href="http://www.xxx.org">http://www.xxx.org</a>
 *
 * \author		ruki
 * \date		09.11.14
 * \file		address.h
 * \brief 		the address of hardware for s3c2440
 *
 */
#ifndef TI_ARCH_ARM_CPU_S3C2440_ADDR_H
#define TI_ARCH_ARM_CPU_S3C2440_ADDR_H

/* ////////////////////////////////////////////////////////////
 * includes
 */
#include "../../../config/config.h"

/* ////////////////////////////////////////////////////////////
 * the watch-dog register
 */
#define TI_S3C2440_rWTCON 			(0x53000000)	// watch-dog timer mode
#define TI_S3C2440_rWTDAT			(0x53000004)	// watch-dog timer data
#define TI_S3C2440_rWTCNT			(0x53000008)	// watch-dog timer count

/* ////////////////////////////////////////////////////////////
 * interrupt control register
 */
#define TI_S3C2440_rSRCPND      	(0x4a000000)	// interrupt rest status
#define TI_S3C2440_rINTMOD			(0x4a000004)	// interrupt mode control
#define TI_S3C2440_rINTMSK     		(0x4a000008)	// interrupt mask control
#define TI_S3C2440_rPRIORITY  		(0x4a00000c)	// irq priority control
#define TI_S3C2440_rINTPND     		(0x4a000010)	// interrupt request status
#define TI_S3C2440_rINTOFFSET		(0x4a000014)	// interrupt request source offset
#define TI_S3C2440_rSUBSRCPND    	(0x4a000018)	// sub source pending
#define TI_S3C2440_rINTSUBMSK 		(0x4a00001c)	// interrupt sub mask

/* ////////////////////////////////////////////////////////////
 * clock & power manager register
 */
#define TI_S3C2440_rLOCKTIME		(0x4c000000)	// pll lock time counter
#define TI_S3C2440_rMPLLCON 		(0x4c000004) 	// mpll control
#define TI_S3C2440_rUPLLCON 		(0x4c000008)	// upll control
#define TI_S3C2440_rCLKCON  		(0x4c00000c)	// clock generator control
#define TI_S3C2440_rCLKSLOW 		(0x4c000010)	// slow clock control
#define TI_S3C2440_rCLKDIVN 		(0x4c000014) 	// clock divider control
#define TI_S3C2440_rCAMDIVN			(0x4c000018)	// camera clock divider


/* //////////////////////////////////////////////////////////////////
 * i/o port register
 */
#define TI_S3C2440_rGPACON    		(0x56000000)	// port A control
#define TI_S3C2440_rGPADAT 			(0x56000004)	// port A data

#define TI_S3C2440_rGPBCON 			(0x56000010)	// port B control
#define TI_S3C2440_rGPBDAT 			(0x56000014)	// port B data
#define TI_S3C2440_rGPBUP 			(0x56000018)	// pull-up control B

#define TI_S3C2440_rGPCCON 			(0x56000020)	// port C control
#define TI_S3C2440_rGPCDAT 			(0x56000024)	// port C data
#define TI_S3C2440_rGPCUP 			(0x56000028)	// pull-up control C

#define TI_S3C2440_rGPDCON 			(0x56000030)	// port D control
#define TI_S3C2440_rGPDDAT 			(0x56000034)	// port D data
#define TI_S3C2440_rGPDUP 			(0x56000038)	// pull-up control D

#define TI_S3C2440_rGPECON 			(0x56000040)	// port E control
#define TI_S3C2440_rGPEDAT 			(0x56000044)	// port E data
#define TI_S3C2440_rGPEUP 			(0x56000048)	// pull-up control E
	
#define TI_S3C2440_rGPFCON 			(0x56000050)	// port F control
#define TI_S3C2440_rGPFDAT 			(0x56000054)	// port F data
#define TI_S3C2440_rGPFUP 			(0x56000058)	// pull-up control F

#define TI_S3C2440_rGPGCON 			(0x56000060)	// port G control
#define TI_S3C2440_rGPGDAT 			(0x56000064)	// port G data
#define TI_S3C2440_rGPGUP 			(0x56000068)	// pull-up control G

#define TI_S3C2440_rGPHCON 			(0x56000070)	// port H control
#define TI_S3C2440_rGPHDAT 			(0x56000074)	// port H data
#define TI_S3C2440_rGPHUP 			(0x56000078)	// pull-up control H

#define TI_S3C2440_rGPJCON 			(0x560000d0)	// port J control
#define TI_S3C2440_rGPJDAT 			(0x560000d4)	// port J data
#define TI_S3C2440_rGPJUP 			(0x560000d8)	// pull-up control J

/* //////////////////////////////////////////////////////////////////
 * uart register
 */
#define TI_S3C2440_rULCON0			(0x50000000)	// uart 0 line control
#define TI_S3C2440_rUCON0			(0x50000004)	// uart 0 control
#define TI_S3C2440_rUFCON0			(0x50000008)	// uart 0 fifo control
#define TI_S3C2440_rUMCON0			(0x5000000c)	// uart 0 modem control
#define TI_S3C2440_rUTRSTAT0		(0x50000010)	// uart 0 tx/rx status
#define TI_S3C2440_rUERSTAT0		(0x50000014)	// uart 0 rx error status
#define TI_S3C2440_rUFSTAT0			(0x50000018)	// uart 0 fifo status
#define TI_S3C2440_rUMSTAT0			(0x5000001c)	// uart 0 modem status
#define TI_S3C2440_rUBRDIV0			(0x50000028)	// uart 0 baud rate divisor
#define TI_S3C2440_rULCON1			(0x50004000)	// uart 1 line control
#define TI_S3C2440_rUCON1			(0x50004004)	// uart 1 control
#define TI_S3C2440_rUFCON1			(0x50004008)	// uart 1 fifo control
#define TI_S3C2440_rUMCON1			(0x5000400c)	// uart 1 modem control
#define TI_S3C2440_rUTRSTAT1		(0x50004010)	// uart 1 tx/rx status
#define TI_S3C2440_rUERSTAT1		(0x50004014)	// uart 1 rx error status
#define TI_S3C2440_rUFSTAT1			(0x50004018)	// uart 1 fifo status
#define TI_S3C2440_rUMSTAT1			(0x5000401c)	// uart 1 modem status
#define TI_S3C2440_rUBRDIV1			(0x50004028)	// uart 1 baud rate divisor
#define TI_S3C2440_rULCON2			(0x50008000)	// uart 2 line control
#define TI_S3C2440_rUCON2			(0x50008004)	// uart 2 control
#define TI_S3C2440_rUFCON2			(0x50008008)	// uart 2 fifo control
#define TI_S3C2440_rUMCON2			(0x5000800c)	// uart 2 modem control
#define TI_S3C2440_rUTRSTAT2		(0x50008010)	// uart 2 tx/rx status
#define TI_S3C2440_rUERSTAT2		(0x50008014)	// uart 2 rx error status
#define TI_S3C2440_rUFSTAT2			(0x50008018)	// uart 2 fifo status
#define TI_S3C2440_rUMSTAT2			(0x5000801c)	// uart 2 modem status
#define TI_S3C2440_rUBRDIV2			(0x50008028)	// uart 2 baud rate divisor

// big endian
#if TI_CONFIG_CPU_BIG_ENDIAN 

#	define TI_S3C2440_rUTXH0		(0x50000023)	// uart 0 transmission hold
#	define TI_S3C2440_rURXH0		(0x50000027)	// uart 0 receive buffer
#	define TI_S3C2440_rUTXH1		(0x50004023)	// uart 1 transmission hold
#	define TI_S3C2440_rURXH1		(0x50004027)	// uart 1 receive buffer
#	define TI_S3C2440_rUTXH2		(0x50008023)	// uart 2 transmission hold
#	define TI_S3C2440_rURXH2		(0x50008027)	// uart 2 receive buffer

// little endian
#else 

#	define TI_S3C2440_rUTXH0		(0x50000020)	// uart 0 transmission hold
#	define TI_S3C2440_rURXH0 		(0x50000024)	// uart 0 receive buffer
#	define TI_S3C2440_rUTXH1 		(0x50004020)	// uart 1 transmission hold
#	define TI_S3C2440_rURXH1 		(0x50004024)	// uart 1 receive buffer
#	define TI_S3C2440_rUTXH2 		(0x50008020)	// uart 2 transmission hold
#	define TI_S3C2440_rURXH2 		(0x50008024)	// uart 2 receive buffer

#endif




#endif

