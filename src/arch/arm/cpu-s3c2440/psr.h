/* the ti-x embed operation system
 * 
 * it is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * it is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with it; If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>
 * 
 * Copyright (C) 2009, Ruki All rights reserved.
 * Home: <a href="http://www.xxx.org">http://www.xxx.org</a>
 *
 * \author		ruki
 * \date		09.11.22
 * \file		psr.h
 * \brief 		the program status registers
 *
 */
#ifndef TI_ARCH_ARM_CPU_S3C2440_PSR_H
#define TI_ARCH_ARM_CPU_S3C2440_PSR_H

/* ////////////////////////////////////////////////////////////
 * includes
 */

/* ////////////////////////////////////////////////////////////
 * the program status registers, PSRs
 *
 * the arm920t contains a current program status register (cpsr), plus five saved program status registers
 * (spsrs) for use by exception handlers. these register's functions are:
 * - hold information about the most recently performed alu operation
 * - control the enabling and disabling of interrupts
 * - set the processor operating mode
 *
 *  Condition Code Flags           (Resverved)                  Control Bits
 *    31  30  29  28         27  26  25  24  23 ... 8       7  6  5  4  3  2  1  0
 *    N   Z   C   V                                         I  F  T  M4 M3 M2 M1 M0
 *    |   |   |   |                                         |  |  |  |____________|
 *    |   |   |   '-- overflow                              |  |  |         |
 *    |   |   '-- carry / borrow/ extend                    |  |  |      mode bits
 *    |   '-- zero                                          |  |  '-- state bits
 *    '-- negative / less than                              |  '-- fiq disable
 *                                                          '--irq disable
 */

// irq & fiq disable bit
#define TI_S3C2440_PSR_IRQ				(0x80)		// when i bit is set irq is disabled
#define TI_S3C2440_PSR_FIQ				(0x40)		// when f bit is set fiq is disabled

// psr mode
#define TI_S3C2440_PSR_MODE_USR			(0x10)		// 10000
#define TI_S3C2440_PSR_MODE_FIQ			(0x11)		// 10001
#define TI_S3C2440_PSR_MODE_IRQ			(0x12)		// 10010
#define TI_S3C2440_PSR_MODE_SVC			(0x13)		// 10011
#define TI_S3C2440_PSR_MODE_ABT			(0x17)		// 10111
#define TI_S3C2440_PSR_MODE_UND			(0x1b)		// 11011
#define TI_S3C2440_PSR_MODE_SYS			(0x1f)		// 11111


#endif

