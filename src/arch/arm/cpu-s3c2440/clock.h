/* the ti-x embed operation system
 * 
 * it is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * it is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with it; If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>
 * 
 * Copyright (C) 2009, Ruki All rights reserved.
 * Home: <a href="http://www.xxx.org">http://www.xxx.org</a>
 *
 * \author		ruki
 * \date		09.11.15
 * \file		clock.h
 * \brief 		the clock config
 *
 */
#ifndef TI_ARCH_ARM_CPU_S3C2440_CLOCK_H
#define TI_ARCH_ARM_CPU_S3C2440_CLOCK_H


/* ////////////////////////////////////////////////////////////
 * includes
 */
#include "prefix.h"

/* ////////////////////////////////////////////////////////////
 * macros
 */

// external crystal frequency, 12MHz, 16MHz ...
#define TI_S3C2440_CLOCK_XTAL				(12000000)

// input frequency = CLOCK_XTAL
#define TI_S3C2440_CLOCK_FIN				(TI_S3C2440_CLOCK_XTAL)

#if TI_CONFIG_S3C2440_CLOCK

/*!mpll(main phase locked loop) - for fclk & hclk & pclk
 *
 * fin: 12MHz
 * fout = (2 * m * fin) / (p * (2 ^ s))
 * m = mdiv + 8
 * p = pdiv + 2
 * s = sdiv
 *
 * PLLCON: 1111 11110011 11110011
 * mdiv:   |-------|
 * pdiv:              |-----|
 * sdiv:                       ||
 *
 * fout			fin		mdiv	pdiv	sdiv
 * 200MHz		12MHz	92		4		1
 * 300MHz		12MHz	67		1		1
 * 400MHz		12MHz	92		1		1
 * 440MHz		12MHz	102		1		1
 * 532MHz		12MHz	125		1		1
 *
 *
 * hdivn	pdivn	1/fclk : 1/hclk : 1/pclk	1/hclk : 1/pclk		hclk3_half/hclk4_half(for camdivn)
 * 0		0			1 : 1 : 1					1 : 1				-
 * 0		1			1 : 1 : 2					1 : 2				-
 * 1		0			1 : 2 : 2					1 : 1				-
 * 1		1			1 : 2 : 4					1 : 2				-
 * 3		0			1 : 3 : 3					1 : 1				0/0
 * 3		1			1 : 3 : 6					1 : 2				0/0
 * 3		0			1 : 6 : 6					1 : 1				1/0
 * 3		1			1 : 6 : 12					1 : 2				1/0
 * 2		0			1 : 4 : 4					1 : 1				0/0
 * 2		1			1 : 4 : 8					1 : 2				0/0
 * 2		0			1 : 8 : 8					1 : 1				0/1
 * 2		1			1 : 8 : 16					1 : 2				0/1
 *
 * \note 1 : 8 : 16 <=> fclk fclk/8 fclk/16
 *
 * FCLK is used by ARM920T.
 *
 * HCLK is used for AHB bus, which is used by the ARM920T, 
 * the memory controller, the interrupt controller, the LCD controller, the DMA and USB host block.
 *
 * PCLK is used for APB bus, which is used by the peripherals 
 * such as WDT, IIS, I2C, PWM timer, MMC interface, ADC, UART, GPIO, RTC and SPI.
 * 
 * The S3C2440A supports selection of Dividing Ratio between FCLK, HLCK and PCLK. 
 * This ratio is determined by HDIVN and PDIVN of CLKDIVN control register
 *
 * \note If HDIVN is not 0, the CPU bus mode has to be changed from the fast bus mode to the asynchronous
 * bus mode using following instructions(S3C2440 does not support synchronous bus mode).
 * \code
	MMU_SetAsyncBusMode
	mrc p15,0,r0,c1,c0,0
	orr r0,r0,#R1_nF:OR:R1_iA
	mcr p15,0,r0,c1,c0,0
 * \endcode
 *
 * If HDIVN is not 0 and the CPU bus mode is the fast bus mode, the CPU will operate by the HCLK.
 * This feature can be used to change the CPU frequency as a half or more without affecting the HCLK and PCLK.
 *
 * e.g.
 *
 * mdiv : pdiv : sdiv = 92 : 1 : 1
 * fout = (2 * (92 + 8) * 12) / ((1 + 2) * (2 ^ 1)) = 400MHz => fclk
 * MPLLCON = ((mdiv & 0xff) << 12) | ((pdiv & 0x3f) << 4) | (sdiv & 0x3) = (92 << 12) | (1 << 4) | 1 = 0101 1100 0000 0001 0001 = 0x0005c011
 * so fclk = 400MHz
 *
 * fclk 	: hclk 		: pclk 		= 400 : 100 : 50 = 8 : 2 : 1
 * 1/fclk 	: 1/hclk 	: 1/pclk 	= 1/400 : 1/100 : 1/50 = 1 : 4 : 8
 * hdivn 	: pdivn 				= 2 : 1
 *
 * CLKDIVN = ((hdivn & 0x3) << 1) | (pdivn & 0x1) = (2 << 1) | 1 = 100 1 = 0x00000005
 *
 * 1/fclk : 1/hclk = 1 : 6 => CAMDIVN[8] = 1
 * 1/fclk : 1/hclk = 1 : 8 => CAMDIVN[9] = 1
 *
 *
 */
# 	if TI_CONFIG_S3C2440_CLOCK_CPU_400MHz

# 		define TI_S3C2440_CLOCK_FCLK				(400000000)		// 400MHz
# 		define TI_S3C2440_CLOCK_HCLK				(100000000)		// 100MHz
# 		define TI_S3C2440_CLOCK_PCLK				(50000000)		// 50MHz

# 		define TI_S3C2440_CLOCK_rMPLLCON_VAL		(0x0005c011)	// fclk = 400MHz
# 		define TI_S3C2440_CLOCK_rCLKDIVN_VAL		(0x00000005)	// hclk = 100MHz pclk = 50MHz
# 		define TI_S3C2440_CLOCK_rCAMDIVN_VAL		(0x00000000)

# 	else
# 		error unknown clock frequency
# 	endif


/* upll(usb phase locked loop) - for uclk
 *
 * fout = (m * fin) / (p * 2 ^ s)
 * m = mdiv + 8
 * p = pdiv + 2
 * s = sdiv
 * fin: 12MHz
 *
 * PLLCON: 1111 11110011 11110011
 * mdiv:   |-------|
 * pdiv:              |-----|
 * sdiv:                       ||
 *
 * Fout			Fin		mdiv	pdiv	sdiv
 * 48MHz		12MHz	56		2		2
 * 96MHz		12MHz	56		2		1
 * ...
 *
 * e.g.
 *
 * mdiv : pdiv : sdiv = 56 : 2 : 1
 * fout = ((56 + 8) * 12) / ((2 + 2) * (2 ^ 1)) = 96MHz => uclk
 * UPLLCON = ((mdiv & 0xff) << 12) | ((pdiv & 0x3f) << 4) | (sdiv & 0x3) = (56 << 12) | (2 << 4) | 1 = 0011 1000 0000 0010 0001 = 0x00038021
 *
 * mdiv : pdiv : sdiv = 56 : 1 : 1
 * fout = ((56 + 8) * 12) / ((1 + 2) * (2 ^ 1)) = 48MHz => uclk
 * UPLLCON = ((mdiv & 0xff) << 12) | ((pdiv & 0x3f) << 4) | (sdiv & 0x3) = (56 << 12) | (1 << 4) | 1 = 0011 1000 0000 0001 0001 = 0x00038011
 */
# 	if TI_CONFIG_S3C2440_CLOCK_USB_96MHz

# 		define TI_S3C2440_CLOCK_UCLK				(96000000)		// 96MHz
# 		define TI_S3C2440_CLOCK_rUPLLCON_VAL		(0x00038021)	// uclk = 96MHz

# 	elif TI_CONFIG_S3C2440_CLOCK_USB_48MHz

# 		define TI_S3C2440_CLOCK_UCLK				(48000000)		// 48MHz
# 		define TI_S3C2440_CLOCK_rUPLLCON_VAL		(0x00038011)	// uclk = 96MHz

# 	else
# 		error unknown usb clock frequency
# 	endif

#else
	
# 	define TI_S3C2440_CLOCK_FCLK			TI_S3C2440_CLOCK_FIN
# 	define TI_S3C2440_CLOCK_HCLK			TI_S3C2440_CLOCK_FIN
# 	define TI_S3C2440_CLOCK_PCLK			TI_S3C2440_CLOCK_FIN
# 	define TI_S3C2440_CLOCK_UCLK			(48000000)		// 48MHz

#endif


/* ////////////////////////////////////////////////////////////
 * base & regs
 */

#define TI_S3C2440_CLOCK_BASE 			(0x4c000000)
#define TI_S3C2440_CLOCK_REGS()			((ti_s3c2440_clock_t*)TI_S3C2440_CLOCK_BASE)


#endif

