/* the ti-x embed operation system
 * 
 * it is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * it is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with it; If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>
 * 
 * Copyright (C) 2009, Ruki All rights reserved.
 * Home: <a href="http://www.xxx.org">http://www.xxx.org</a>
 *
 * \author		ruki
 * \date		09.12.05
 * \file		uart.h
 *
 */
#ifndef TI_ARCH_ARM_CPU_S3C2440_UART_H
#define TI_ARCH_ARM_CPU_S3C2440_UART_H


/* ////////////////////////////////////////////////////////////
 * includes
 */
#include "prefix.h"

/* ////////////////////////////////////////////////////////////
 * macros
 */

/* ////////////////////////////////////////////////////////////
 * base & regs
 */

#define TI_S3C2440_UART0_BASE 			(0x50000000)
#define TI_S3C2440_UART1_BASE 			(0x50004000)
#define TI_S3C2440_UART2_BASE 			(0x50008000)

#define TI_S3C2440_UART0_REGS()			((ti_s3c2440_uart_t*)TI_S3C2440_UART0_BASE)
#define TI_S3C2440_UART1_REGS()			((ti_s3c2440_uart_t*)TI_S3C2440_UART1_BASE)
#define TI_S3C2440_UART2_REGS()			((ti_s3c2440_uart_t*)TI_S3C2440_UART2_BASE)
#define TI_S3C2440_UART_REGS(n)			((ti_s3c2440_uart_t*)(TI_S3C2440_UART0_BASE + (0x4000 * (n))))


#endif

