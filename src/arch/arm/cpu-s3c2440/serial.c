/* the ti-x embed operation system
 * 
 * it is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * it is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with it; If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>
 * 
 * Copyright (C) 2009, Ruki All rights reserved.
 * Home: <a href="http://www.xxx.org">http://www.xxx.org</a>
 *
 * \author		ruki
 * \date		09.11.21
 * \file		serial.c
 *
 */

/* ////////////////////////////////////////////////////////////////////////
 * includes
 */
#include "prefix.h"
#include "clock.h"
#include "uart.h"

/* ////////////////////////////////////////////////////////////////////////
 * macros
 */
#if defined(TI_CONFIG_S3C2440_SERIAL0)
# 	define TI_S3C2440_SERIAL_REGS 		TI_S3C2440_UART0_REGS 	// uart0
#elif defined(TI_CONFIG_S3C2440_SERIAL1)
# 	define TI_S3C2440_SERIAL_REGS 		TI_S3C2440_UART1_REGS 	// uart1
#elif defined(TI_CONFIG_S3C2440_SERIAL2)
# 	define TI_S3C2440_SERIAL_REGS 		TI_S3C2440_UART2_REGS 	// uart2
#else
# 	error unknown serial
#endif

/* ////////////////////////////////////////////////////////////////////////
 * details
 */

void ti_s3c2440_serial_init()
{
	ti_s3c2440_uart_t* puart = TI_S3C2440_SERIAL_REGS();

	// uart fifo control register, fifo disable
	puart->rUFCON 		= 0x0; 	

	// uart fifo control register, fifo enable & tx/rx fifo clear
	//puart->rUFCON 		= 0x07; 	

	// uart modem control register, afc disable
	puart->rUMCON 		= 0x0; 	

	/* line control register
	 *
	 *    [7]          [6]         [5:3]            [2]             [1:0]
	 * reserved, infrared mode, parity mode, number of stop bit, word length
	 *     0           0            000              0                11
	 *              normal       no parity        1 stop           8 bits
	 *
	 */
	puart->rULCON 		= 0x3;

	/* control register
	 *
	 *   [15:12]      [11:10]     [9]     [8]        [7]        [6]      [5]         [4]        [3:2]         [1:0]
	 * fclk divider, clock sel, tx int, rx int, rx time out, rx err, loop-back, send break, transmit mode, receive mode
	 *    0000          00         1       0          0          1        0           0           01          01
	 *                 pclk      level    pulse    disable    generate  normal      normal      interrupt or polling
	 *
	 */
	puart->rUCON 		= 0x245;

	/* baud rate divisior register
	 *
	 * UBRDIVn = (int)( UART clock / ( baud rate x 16) ) šC 1
	 * UART clock: PCLK, FCLK/n or UEXTCLK
	 * Where, UBRDIVn should be from 1 to (216 - 1), but can be set zero only using the UEXTCLK which should be smaller than PCLK.
	 *
	 * For example, if the baud-rate is 115200 bps and UART clock is 40 MHz, UBRDIVn is:
	 * UBRDIVn	= (int)(40000000 / (115200 x 16) ) -1
	 *			= (int)(21.7) -1 [round to the nearest whole number]
	 *			= 22 -1 = 21
	 */
	puart->rUBRDIV 		= ((TI_S3C2440_CLOCK_PCLK / (TI_CONFIG_S3C2440_SERIAL_BAUDRATE * 16)) - 1);

	// delay some time
	volatile ti_int_t i = 0;
	for (i = 0; i < 100; i++);
}

void ti_s3c2440_serial_putc(ti_char_t c)
{
	ti_s3c2440_uart_t* puart = TI_S3C2440_SERIAL_REGS();

	// write return
	if (c == '\n') ti_s3c2440_serial_putc('\r');

	// empty?
	while (!(puart->rUTRSTAT & 0x2));

	// write data
	puart->rUTXH = c;
}

ti_char_t ti_s3c2440_serial_getc()
{
	ti_s3c2440_uart_t* puart = TI_S3C2440_SERIAL_REGS();

	// non-empty?
	while (!(puart->rUTRSTAT & 0x1));

	// read data
	return (puart->rURXH & 0xff);
}

