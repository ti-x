/* the ti-x embed operation system
 * 
 * it is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * it is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with it; If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>
 * 
 * Copyright (C) 2009, Ruki All rights reserved.
 * Home: <a href="http://www.xxx.org">http://www.xxx.org</a>
 *
 * \author		ruki
 * \date		09.11.21
 * \file		clock.c
 *
 */

/* ////////////////////////////////////////////////////////////////////////
 * includes
 */
#include "prefix.h"
#include "clock.h"

/* ////////////////////////////////////////////////////////////////////////
 * types
 */

/* ////////////////////////////////////////////////////////////////////////
 * details
 */

// at least 7-clocks delay must be inserted for setting hardware be completed.
static inline void ti_s3c2440_clock_delay_7nops()
{
    __asm__ volatile 	( 	"nop\n"
							"nop\n"
							"nop\n"
							"nop\n"
							"nop\n"
							"nop\n"
							"nop\n"
						);
}

static inline void ti_s3c2440_clock_delay(ti_int_t loops)
{
 	__asm__ volatile 	( 	"1:\n"
 							"subs %0, %1, #1\n"
 							"bne 1b"
							:"=r" (loops):"0" (loops)
						);
}

#if TI_CONFIG_S3C2440_CLOCK
void ti_s3c2440_clock_init()
{
	ti_s3c2440_clock_t* pclock = TI_S3C2440_CLOCK_REGS();
	pclock->rLOCKTIME 	= 0x0fff0fff; // to reduce PLL lock time
	pclock->rCLKCON 	= 0x001ffff0;
	pclock->rCLKSLOW 	= 0x00000004;
	pclock->rCLKDIVN 	= TI_S3C2440_CLOCK_rCLKDIVN_VAL;
	pclock->rCAMDIVN 	= TI_S3C2440_CLOCK_rCAMDIVN_VAL;

	pclock->rMPLLCON 	= TI_S3C2440_CLOCK_rMPLLCON_VAL;
	//ti_s3c2440_clock_delay_7nops();
	ti_s3c2440_clock_delay(4000);

	pclock->rUPLLCON 	= TI_S3C2440_CLOCK_rUPLLCON_VAL;
	//ti_s3c2440_clock_delay_7nops();
	ti_s3c2440_clock_delay(4000);

# 	if (TI_S3C2440_CLOCK_rCAMDIVN_VAL > 1)  // fclk : hclk != 1 : 1
		// change to asynchronous bus mode
		__asm__ (   "mrc 	p15, 0, r0, c1, c0, 0\n"
					"orr 	r0, r0, #0xc0000000\n" 	// R1_nF:OR:R1_iA
					"mcr 	p15, 0, r0, c1, c0, 0\n"
					:::"r0"
				);
# 	else
		// change to fast bus mode
		__asm__ (   "mrc 	p15, 0, r0, c1, c0, 0\n"
					"bic 	r0, r0, #0xc0000000\n" 	// R1_iA:OR:R1_nF
					"mcr 	p15, 0, r0, c1, c0, 0\n"
					:::"r0"
				);
# 	endif
}
#endif


