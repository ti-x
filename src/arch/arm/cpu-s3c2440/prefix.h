/* the ti-x embed operation system
 * 
 * it is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * it is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with it; If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>
 * 
 * Copyright (C) 2009, Ruki All rights reserved.
 * Home: <a href="http://www.xxx.org">http://www.xxx.org</a>
 *
 * \author		ruki
 * \date		09.11.21
 * \file		prefix.h
 * \brief 		the prefix file of cpu
 *
 */
#ifndef TI_ARCH_ARM_CPU_S3C2440_PREFIX_H
#define TI_ARCH_ARM_CPU_S3C2440_PREFIX_H

/* ////////////////////////////////////////////////////////////////////////
 * includes
 */
#include "../prefix.h"


/* ////////////////////////////////////////////////////////////////////////
 * types
 */
typedef volatile ti_uint8_t 	ti_s3c2440_reg8_t;
typedef volatile ti_uint16_t 	ti_s3c2440_reg16_t;
typedef volatile ti_uint32_t 	ti_s3c2440_reg32_t;

// clock & power register
typedef struct __ti_s3c2440_clock_t
{
	ti_s3c2440_reg32_t 	rLOCKTIME;
	ti_s3c2440_reg32_t 	rMPLLCON;
	ti_s3c2440_reg32_t 	rUPLLCON;
	ti_s3c2440_reg32_t 	rCLKCON;
	ti_s3c2440_reg32_t 	rCLKSLOW;
	ti_s3c2440_reg32_t 	rCLKDIVN;
	ti_s3c2440_reg32_t 	rCAMDIVN;

}ti_s3c2440_clock_t;

// uart register
typedef struct __ti_s3c2440_uart_t
{
	ti_s3c2440_reg32_t 	rULCON;
	ti_s3c2440_reg32_t 	rUCON;
	ti_s3c2440_reg32_t 	rUFCON;
	ti_s3c2440_reg32_t 	rUMCON;
	ti_s3c2440_reg32_t 	rUTRSTAT;
	ti_s3c2440_reg32_t 	rUERSTAT;
	ti_s3c2440_reg32_t 	rUFSTAT;
	ti_s3c2440_reg32_t 	rUMSTAT;

	// big endian
#if TI_CONFIG_CPU_BIG_ENDIAN

	ti_s3c2440_reg8_t 	res1[3]; // reserved
	ti_s3c2440_reg8_t 	rUTXH;
	ti_s3c2440_reg8_t 	res2[3]; // reserved
	ti_s3c2440_reg8_t 	rURXH;

	// little endian
#else 

	ti_s3c2440_reg8_t 	rUTXH;
	ti_s3c2440_reg8_t 	res1[3]; // reserved
	ti_s3c2440_reg8_t 	rURXH;
	ti_s3c2440_reg8_t 	res2[3]; // reserved

#endif

	ti_s3c2440_reg32_t 	rUBRDIV;

}ti_s3c2440_uart_t;


#endif
