#include "prefix.h"

int main(int argc, char** argv)
{
#if 1
	ti_printf("|hello world|\n");
	ti_printf("|%-10s|%%|%10s|\n", "hello", "world");
	ti_printf("|%#2c|%2.5c|%*c|\n", 'A', 'B', 5, 'C');
	ti_printf("|%#2d|%#8.3o|%*.*d|\n", -56, 56, 10, 5, 56);
	ti_printf("|%#-8.5x|%#2.9X|\n", 0x1f, 0x1f);
	ti_printf("|%#-8.5b|%#2.9B|\n", 0x1f, 0x1f);
	ti_printf("|%-6Id|%5I8u|%#I64x|%#llx|\n", 256, 255, (ti_int64_t)0x8fffffffffff, (ti_int64_t)0x8fffffffffff);
	ti_printf("|%f|\n", -3.1415926535897932384626433832795);
	ti_printf("|%f|%f|%f|\n", 3.14, 0, -0);
	ti_printf("|%0.9f|\n", 3.1415926535897932384626433832795);
	ti_printf("|%16.9f|\n", 3.1415926535897932384626433832795);
	ti_printf("|%016.9f|\n", 3.14159);
	ti_printf("|%lf|\n", 1.0 / 6.0);
	ti_printf("|%f|\n", 0.0003141596);
	ti_printf("|%s|\n", TI_GCC_VERSION_STRING);
#endif
	return 0;
}

