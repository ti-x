/* the ti-x embed operation system
 * 
 * it is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * it is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with it; If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>
 * 
 * Copyright (C) 2009, Ruki All rights reserved.
 * Home: <a href="http://www.xxx.org">http://www.xxx.org</a>
 *
 * \author		ruki
 * \date		09.11.21
 * \file		prefix.h
 * \brief 		the prefix file of ti-x
 *
 */
#ifndef TI_PREFIX_H
#define TI_PREFIX_H

/* ////////////////////////////////////////////////////////////////////////
 * includes
 */

// config
#include "config/config.h"
#include "arch/support.h"

/* ////////////////////////////////////////////////////////////////////////
 * the basic macros
 */
// invalidate handle value
#define TI_INVALID_HANDLE 	((ti_handle_t)(-1))

// return value
#define	TI_TRUE				((ti_bool_t)(1))
#define	TI_FALSE			((ti_bool_t)(0))

// NULL
#define TI_NULL 			((void*)0)

/* ////////////////////////////////////////////////////////////////////////
 * the gcc version
 */

#define TI_GCC_VERSION_MAJOR 			(__GNUC__)
#define TI_GCC_VERSION_MINOR 			(__GNUC_MINOR__)

#if  __GNUC__ == 2

#	if __GNUC_MINOR__ < 95
#		define TI_GCC_VERSION_STRING		"GNU C/C++ <2.95"
#	elif __GNUC_MINOR__ == 95
#		define TI_GCC_VERSION_STRING		"GNU C/C++ 2.95"
#	elif __GNUC_MINOR__ == 96
#		define TI_GCC_VERSION_STRING		"GNU C/C++ 2.96"
#	else
#		define TI_GCC_VERSION_STRING		"GNU C/C++ >2.96&&<3.0"
#	endif

#elif __GNUC__ == 3

#	if __GNUC_MINOR__ == 2
#		define TI_GCC_VERSION_STRING		"GNU C/C++ 3.2"
#	elif __GNUC_MINOR__ == 3
#		define TI_GCC_VERSION_STRING		"GNU C/C++ 3.3"
#	elif __GNUC_MINOR__ == 4
#		define TI_GCC_VERSION_STRING		"GNU C/C++ 3.4"
#	else
#		define TI_GCC_VERSION_STRING		"GNU C/C++ >3.4&&<4.0"
#	endif

#elif __GNUC__ == 4

#	if __GNUC_MINOR__ == 1
#		define TI_GCC_VERSION_STRING		"GNU C/C++ 4.1"
#	elif __GNUC_MINOR__ == 2
#		define TI_GCC_VERSION_STRING		"GNU C/C++ 4.2"
#	elif __GNUC_MINOR__ == 3
#		define TI_GCC_VERSION_STRING		"GNU C/C++ 4.3"
#	elif __GNUC_MINOR__ == 4
#		define TI_GCC_VERSION_STRING		"GNU C/C++ 4.4"
#	endif

#else
#	error Unknown GNU C/C++ Compiler Version
#endif


/* ////////////////////////////////////////////////////////////////////////
 * the basic types
 */
typedef signed int			ti_int_t;
typedef unsigned int		ti_uint_t;
typedef ti_int_t			ti_bool_t;
typedef ti_uint_t			ti_size_t;
typedef signed char			ti_int8_t;
typedef unsigned char		ti_uint8_t;
typedef signed short		ti_int16_t;
typedef unsigned short		ti_uint16_t;
typedef ti_int_t			ti_int32_t;
typedef ti_uint_t			ti_uint32_t;
typedef ti_uint8_t			ti_byte_t;
typedef char				ti_char_t;
typedef double 				ti_float_t;
typedef void* 				ti_handle_t;

#ifdef TI_ARCH_HAVE_INT64
typedef signed long long	ti_int64_t;
typedef unsigned long long	ti_uint64_t;
#else
typedef ti_int32_t 			ti_int64_t;
typedef ti_uint32_t 		ti_uint64_t;
#endif




// TI_PREFIX_H
#endif


