# main makefile

# #####################################################
# includes
# #
include config.mak

# #####################################################
# make projects
# #
ifeq ($(IS_CONFIG), yes)

# include prefix
include prefix.mak

# make all
all : .null
	$(MAKE) -C $(SRC_DIR)

# make install
install : .null
	-$(RMDIR) $(BIN_DIR)
	-$(MKDIR) $(BIN_DIR)
	-$(RMDIR) $(BIN_DIR)$(_)inc
	-$(RMDIR) $(BIN_DIR)$(_)lib
	-$(RMDIR) $(BIN_DIR)$(_)obj
	-$(MKDIR) $(BIN_DIR)$(_)inc
	-$(MKDIR) $(BIN_DIR)$(_)lib
	-$(MKDIR) $(BIN_DIR)$(_)obj
	-$(CPDIR) $(SRC_DIR)$(_)config $(BIN_DIR)$(_)inc
	$(MAKE) -C $(SRC_DIR) install

# make clean
clean : .null
	$(MAKE) -C $(SRC_DIR) clean

.null :

else

# #####################################################
# no-config
# #
all :
	@echo "please make config..."

endif

# #####################################################
# config
# #

# default args
PLAT ?= linux_x86
HOST ?= linux

ifeq ($(HOST), linux)
config :
	# generate config.h
	-rm -r ${shell pwd}$(_)src$(_)config
	-cp -r ${shell pwd}$(_)plat$(_)$(PLAT)$(_)config ${shell pwd}$(_)src$(_)config

	# generate config.mak
	@echo "# config"                      	> config.mak
	@echo "IS_CONFIG =  yes" 				>> config.mak
	@echo ""                              	>> config.mak
	@echo "# architecture"                	>> config.mak
	@echo "PLAT = " $(PLAT) 				>> config.mak
	@echo ""                              	>> config.mak
	@echo "# host"                			>> config.mak
	@echo "HOST = " $(HOST) 				>> config.mak
	@echo ""                              	>> config.mak
	@echo "# root"                			>> config.mak
	@echo "PRO_DIR = " ${shell pwd} 		>> config.mak
	@echo ""                              	>> config.mak
	@echo "# export"						>> config.mak
	@echo "export PLAT"					 	>> config.mak
	@echo "export HOST" 		 			>> config.mak
	@echo "export PRO_DIR" 		 			>> config.mak

endif

ifeq ($(HOST), windows)
config :
	-del /Q /P ${shell chdir}$(_)src$(_)config
	-copy ${shell chdir}$(_)plat$(_)$(PLAT)$(_)config ${shell chdir}$(_)src$(_)config
	@echo IS_CONFIG = yes> config.mak
	@echo PLAT = $(PLAT)>> config.mak
	@echo HOST = $(HOST)>> config.mak
	@echo PRO_DIR = ${shell chdir}>> config.mak
	@echo export PLAT>> config.mak
	@echo export HOST>> config.mak
	@echo export PRO_DIR>> config.mak

endif
